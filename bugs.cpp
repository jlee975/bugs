/** This is a series of tests for Insure, Address-Sanitizer,
 *  and Valgrind. Each function demonstrates a bug that "ought"
 *  to be caught by the tool in question.
 *
 * Tests will need to be enabled individually, since they pretty
 * much all crash the program.
 *
 * How to compile and run:
 *
 * Insure:
 *   insure g++ -O0 -g -ggdb3 bugs.cpp -o bugs
 *   ./bugs
 *
 * Address Sanitizer (Requires GCC >= 4.8):
 *   g++ -O0 -g -ggdb3 -fsanitize=address -static-libasan bugs.cpp -o bugs
 *
 * Asan with Ubsan (Requires GCC >= 4.9)
 *   g++ -O0 -g -ggdb3 -fsanitize=address -fsanitize=pointer-compare -fsanitize=pointer-subtract -fsanitize=leak -fsanitize=undefined -fno-omit-frame-pointer -static-libasan -static-libubsan -static-liblsan bugs.cpp -o bugs
 *   ASAN_OPTIONS=detect_stack_use_after_return=1 UBSAN_OPTIONS=print_stacktrace=1 ./bugs
 *
 * Valgrind
 *   g++ -O0 -g -ggdb3 bugs.cpp -o bugs
 *   valgrind --tool=memcheck -v --read-var-info=yes --track-origins=yes --show-reachable=yes --leak-check=full --read-inline-info=yes --keep-stacktraces=alloc-and-free bugs
 *
 */
#include <cstdlib>
#include <iostream>
#include <new>
#include <cstring>
#include <climits>
#include <cstdio>
#include <string>
#include <climits>
#include <vector>

// =============================================================================
// Simply ensures that the array is not optimized out
void fill(int* p, int size)
{
	// fill
	for (int i = 0; i < size; ++i)
		p[i] = i;
	// scramble
	for (int i = 0; i< 1000; ++i)
		p[rand() % size] = i;
}

// =============================================================================
// Try to free something that was never initialized
void delete_uninitialized()
{
	int* p;
	delete p;
}

// =============================================================================
// p isn't initialized before it is used
void wild_pointer()
{
	int* p;
	fill(p, 100);
	std::cout << p[0] << std::endl;
}

// =============================================================================
// null pointer is dereferenced
void null_pointer()
{
	int* p = 0;
	fill(p, 100);
	std::cout << p[0] << std::endl;
}

// =============================================================================
// overrun a file scope array
int globalp[100] = { };

void global_overflow()
{
	fill(globalp, 100);
	for (int i = 0; i < 100; ++i)
		globalp[i] += globalp[i + 1];
}

// =============================================================================
// returns a local array so it can then be abused
int* use_local_array_helper()
{
	int p[100];
	fill(p, 100);
	int* q = p; // stop gcc from seeing the problem
	return q;
}

// Uses a stack array that has gone out of scope
void use_local_array()
{
	std::cout << use_local_array_helper()[99] << std::endl;
}

// =============================================================================
// Use uninitialized data in heap
void read_uninitialized_heap()
{
	int* p = new int[128];
	fill(p, 127);
	
	for (int i = 0; i < 127; ++i)
		p[i] += p[i + 1];
	std::cout << p[126] << std::endl;
	delete[] p;
}

// =============================================================================
// Use uninitialized data in stack
void read_uninitialized_stack()
{
	int p[128];
	fill(p, 127);
	
	for (int i = 0; i < 127; ++i)
		p[i] += p[i + 1];
	std::cout << p[126] << std::endl;
	
}

// =============================================================================
// Use an uninitialized value. Passing indirectly through pointer to a library
// call to make this extra difficult to catch.
size_t use_uninitialized_value(const char* p)
{
	return strlen(p);
}

void pass_uninitialized_value()
{
	char pad1[] = "Not empty";
	char x[1024];
	char pad2[] = "Empty";

	
	if (use_uninitialized_value(x) == 0)
		std::cout << pad2 << std::endl;
	else
		std::cout << pad1 << std::endl;
}

// =============================================================================
// Overrun a stack array (that is padded)
void stack_overflow()
{
	int before[128];
	int p[128];
	int after[128];

	fill(before, 128);
	fill(p, 128);
	fill(after, 128);

	for (int i = 0; i < 128; ++i)
		p[i] += p[i + 1];
	std::cout << p[99] << std::endl;
}

// =============================================================================
// Overrun memory allocated at run time
void heap_overflow()
{
	int* p = new int[100];
	fill(p, 100);
	for (int i = 0; i < 100; ++i)
		p[i] += p[i + 1];
	std::cout << p[99] << std::endl;
	delete[] p;
}

// =============================================================================
// Use free instead of delete[]
void wrong_free()
{
	int* p = new int[100];
	fill(p, 100);
	free(p);
}

// =============================================================================
// Use delete instead of delete[]
void wrong_delete()
{
	int* p = new int[100];
	fill(p, 100);
	delete p;
}

// =============================================================================
// Free a block of memory twice
void double_free()
{
	int* p = new int[100];
	fill(p, 100);
	delete[] p;
	delete[] p;
}

// =============================================================================
// Don't free allocated memory
void leak()
{
	int* p = new int[100];
	fill(p, 100);
	std::cout << p[0] << std::endl;
}

// =============================================================================
// Free memory, then try to use it
void use_after_free()
{
	int* p = new int[100];
	delete[] p;
	fill(p, 100);
}

// =============================================================================
// Division by zero is undefined behavior
void divide_by_zero()
{
	int x = 0;
	int y = 3/x;
}

// =============================================================================
// (signed) integer arithmetic is not allowed to overflow in C/C++
void integer_overflow()
{
	int x = INT_MAX;
	++x;
}

// =============================================================================
// Function does not have a return statement
int no_return()
{
	int x = 3;
	int y = x * 97;
}

// =============================================================================
// string is not null-terminated, so strlen overflows. Using
// the built-in version of strlen to encourage use of repne or
// similar instruction to be issued.
void runaway_strlen()
{
	char before[] = "Before";
	char buf[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };
	char after[] = "After";

#ifdef __GNUC__
	std::cout << "Using built-in strlen" << std::endl;
	std::cout << __builtin_strlen(buf) << std::endl;
#else
	std::cout << "Not using built-in strlen" << std::endl;
	std::cout << strlen(buf) << std::endl;
#endif
}

// =============================================================================
// p is shorter than q, but memcmp is passed length of q
// Using the built-in version of memcmp to encourage use of
// repe or similar instruction
void runaway_memcmp()
{
	int* p = new int[4095];
	int* q = new int[4096];
	fill(q, 4096);
	for (int i = 0; i < 4095; ++i)
		p[i] = q[i];

#ifdef __GNUC__
	int res = __builtin_memcmp(p, q, 4096 * sizeof(int));
#else
	int res = memcmp(p, q, 4096 * sizeof(int));
#endif
	std::cout << "result of runaway memcmp = " << res << std::endl;
	delete[] p;
	delete[] q;
}

// =============================================================================
// Try to copy a class with memcpy
void memcpy_non_pod()
{
	std::string s("Hello");
	void* p = malloc(sizeof(s));
	memcpy(p, &s, sizeof(s));
	free(p);
}

// =============================================================================
// Mismatch of a printf formatting string and varargs
void bad_printf_inner(
		const char* s // s will be "%s %d"
		     )
{
	printf(s, 430429, "Hello");
}

// Exchange string and int
void bad_printf()
{
	std::string fmt;
	fmt += "%s";
	fmt += ' ';
	fmt += "%d";
	bad_printf_inner(fmt.c_str());
}

// =============================================================================
// Only part of t.i is written to (via the union)
union collapse {
	char c[sizeof(int)];
	int i;
};

void partial_union_init()
{
	collapse t;
	for (std::size_t i = 0; i + i < sizeof(int); ++i)
		t.c[i] = static_cast<char>(i);
	std::cout << t.i << std::endl;
}

// =============================================================================
// Legally, can only read from last member written to. The
// read from bitreader::c is disallowed by the language.
union bitreader
{
	double x;
	unsigned char c[sizeof(double)];
};

void wrong_union_member()
{
	bitreader temp;
	temp.x = 3.0;
	std::cout << (int)temp.c[sizeof(double) - 1] << std::endl;
}

// =============================================================================
// A base class without a virtual destructor
class Base {
	public:
		~Base() { std::cout << "Base" << std::endl; }
};

// Derived should not be deleted through Base*
class Derived : public Base
{
	public:
		~Derived() { std::cout << "Derived" << std::endl; }
};

// A class that isn't Base
class NotBase
{
	public:
		~NotBase() { std::cout << "NotBase" << std::endl; }
};

// =============================================================================
// Wrong delete is called. Should call ~Base, then
// delete[] buf
void placement_new()
{
	char* buf = new char[sizeof(Base)];
	Base* p = new(buf) Base;
	delete p;
}

// =============================================================================
// Base's destructor is not virtual, so ~Derived is not called
void bad_destructor()
{
	Base* p = new Derived;
	delete p;
}

// =============================================================================
// void pointer has changed the type, so wrong delete is called
void bad_cast()
{
	void* p = new NotBase;
	Base* q = static_cast<Base*>(p);

	delete q;
}

// =============================================================================
// Create a pointer (q) that is not aligned correctly for the
// type. big is sortofa "maximally aligned union" with
// sizeof(big) >= 2. We offset one char from the start of the
// union, which ought to be misalinged for accessing big::d
// in the loop (assuming sizeof(long double) > 1)
union big
{
	char pad[2];
	unsigned long l;
	long double d;
};

void misaligned_ptr()
{
	big* p = new big[11];

	// offet by one byte and cast back to big*
	big* q = reinterpret_cast<big*>(p[0].pad + 1);
	for (int i = 0; i < 10; ++i)
		q[i].d = 3.0;

	std::cout << "Address of correctly aligned pointer:               " << (void*)p << std::endl;
	std::cout << "Address of misaligned pointer (should be off by 1): " << (void*)q << std::endl;
	std::cout << "Address of misaligned big::d (should be same):      " << (void*)(&q[0].d) << std::endl;

	delete[] p;
}

// =============================================================================
// Read an entire word (atomically) at the end of a stack buffer
// that is too short
void load_partial_1()
{
	char p[2 * sizeof(unsigned long) - 1] = { };
	unsigned long* q = reinterpret_cast<unsigned long*>(p);
	std::cout << q[1] << std::endl;
}

// =============================================================================
// Read an entire word (atomically) at the end of a heap buffer
// that is too short
void load_partial_2()
{

	char* p = new char[2 * sizeof(unsigned long) - 1];
	unsigned long* q = reinterpret_cast<unsigned long*>(p);
	std::cout << q[1] << std::endl;
	delete[] p;
}

// =============================================================================
// Overlapping destination and source passed to memcpy
void memcpy_self()
{
	int * p = new int[100];
	fill(p, 100);
	memcpy(p + 10, p, 90);
	delete[] p;
}

// =============================================================================
// A couple different ways that bit fields can be use incorrectly
struct bits {
	int field1 : 7;
	int field2 : 7;
	int field3 : 18;
};

// The multi-byte field3 member of bits is uninitialized
void uninit_bitfield_1()
{
	bits t;
	t.field1 = 3;
	t.field2 = 8;
	std::cout << t.field3 << " : " << (sizeof(bits) * CHAR_BIT) << std::endl;
}

// bits::field1 is less than one byte, and will probably be written to
// when field2 is initialized. So field1 will probably "look" initialized
// even thought it is not.
void uninit_bitfield_2()
{
	bits t;
	t.field2 = 8;
	t.field3 = 3;
	std::cout << t.field1 << " : " << (sizeof(bits) * CHAR_BIT) << std::endl;
}

// =============================================================================
// std::string can use a global variable for empty strings. Consequently, a
// write to an empty string may propogate to other (empty) strings. This bug
// may not show up if the string implementation uses SSO.
//
// Note that this bug is particularly tricky to catch. The debug containers
// provided by libstdc++ do not seem to catch the misuse of operator[]. Because
// there is a null byte there, Asan and others see it as a write to allocated
// memory. And because of the interface of std::string, the array is not marked
// as const, so that doesn't trigger either.
//
// *** Only solution as of this moment is to use GDB and watch std::string().c_str()
void write_to_empty_string()
{
	std::string s;
	std::string t;
	strcpy(&s[0], "!");

	// Need to use c_str to see the problem.
	std::cout << "Should be empty string: '" << t.c_str() << "'" << std::endl;
}

// =============================================================================
// Writing to a string literal can cause other instances of that literal to
// change.
//
// *** Note that modern compilers will put string literals in rodata, and the OS
// will catch the write attempt (resulting in a segfault).
void circumvent_constness_helper()
{
	const char* p = "Hello";
	char* q = const_cast<char*>(p);
	q[3] = 'p';
	q[4] = '\0';
}

void circumvent_constness()
{
	circumvent_constness_helper();
	const char* p = "Hello";
	std::cout << p << std::endl;
}

// =============================================================================
// Access to containers may be illegal even if it's okay at the pointer level.
// Here we reserve and initialize data, but access is illegal after the
// resize. A class is used so that the lost elements are appropriately "cleaned
// up" (at least as far as the langauge is concerned).
class thingy
{
public:
	thingy() : value(0) { }
	thingy(int y) : value(y) { }
	int mul(int x) const
	{
		return value * x;
	}
	
private:
	int value;
};

void vector_access()
{
	std::vector< thingy > v(100, rand());
	v.resize(10);
	std::cout << v[10].mul(rand()) << std::endl;
}

void shl(int x, int y)
{
	std::cout << (x << y) << std::endl;
}

void shr(int x, int y)
{
	std::cout << (x >> y) << std::endl;
}

/* Each of the following shifts is undefined behavior
 */
void bad_shift()
{
	shl(3, 100);
	shl(1, -1);
	shr(1, -1);
	shr(1, 100);
	shl(INT_MAX, 2);
	shr(INT_MIN, 2);
}

void bad_bool()
{
	bool b = true;
	char* p = static_cast< char* >(static_cast< void* >(&b));
	p[0] = 2;
	std::cout << b << std::endl;
}

enum my_enum { ZERO, ONE, TWO };

void bad_enum()
{
	my_enum x = ZERO;
	char* p = static_cast< char* >(static_cast< void* >(&x));
	p[0] = 6;
	std::cout << x << std::endl;
}

void pointer_subtraction()
{
	char p[100];
	char q[100];

	std::cout << ((p + 100) - p) << std::endl;
	std::cout << (q - p) << std::endl;
}
void pointer_comparison()
{
	char p[100];
	char q[100];

	if (p < p + 100)
		std::cout << "OK pointer comparison within array" << std::endl;
	if (p < q)
		std::cout << "Bad pointer comparison between arrays" << std::endl;
}

int main()
{
/* ------------------------------------------------------------------
 * First group of errors are pretty common and ought to be caught.
 * These are things that everyone has done from time to time
 */
//  Type of error                     Insure       Sanitizer    Valgrind   STL
//	leak();                     //      Yes        Yes         Yes      No
//	global_overflow();          //      Yes        Yes          No      No
//	wrong_free();               //      Yes        Yes         Yes      No
//	wrong_delete();             //      Yes        Yes         Yes      No
//	double_free();              //      Yes        Yes         Yes      No
//	use_after_free();           //      Yes        Yes         Yes      No
//	stack_overflow();           //      Yes        Yes          No      No
//	heap_overflow();            //      Yes        Yes         Yes      No
//	delete_uninitialized();     //      Yes        Yes         Yes      No
//	wild_pointer();             //      Yes        Yes         Yes      No
//	null_pointer();             //      Yes        Yes         Yes      No
//	read_uninitialized_heap();  //      Yes         No         Yes      No
//	read_uninitialized_stack(); //      Yes         No         Yes      No
//	use_local_array();          //      Yes        Yes         Yes      No
//	bad_printf();               //      Yes         No         Yes      No
//	runaway_strlen();           //       No        Yes         3.11     No
//	no_return();                //      Yes        Yes          No      No
//	integer_overflow();         //       No        Yes          No      No
//	divide_by_zero();           //       No        Yes          No      No
//	pass_uninitialized_value(); //       No         No         Yes      No
//	#define BAD_INITIALIZATION_ORDER  // No        Yes         Yes      No
//	vector_access();            //       No         No          No     Yes
//	bad_shift();                //       No        Yes          No      No
//	bad_bool();                 //       No        Yes          No      No
//	bad_enum();                 //       No        Yes          No      No
//	pointer_subtraction();      //       ??        Yes          No      ??
//	pointer_comparison();       //       ??        Yes          No      ??

/* ------------------------------------------------------------------
 * Second group of errors are a little contrived, but it would sure
 * be nice if they were caught.
 *
 * They are uncommon, or intentional, abuses of C++.
 *
 * @todo builtin memcpy
 * @todo illegal enums
 */
//  Type of error                     Insure       Sanitizer    Valgrind
//	memcpy_self();              //      Yes        Yes         Yes
//	memcpy_non_pod();           //       No         No          No
//	partial_union_init();       //       No         No         Yes
//	wrong_union_member();       //       No         No          No
//	placement_new();            //       No        Yes         Yes
//	bad_destructor();           //       No         No          No
//	bad_cast();                 //       No         No          No
//	misaligned_ptr();           //       No        Yes          No
//	uninit_bitfield_1();        //       No         No         Yes
//	uninit_bitfield_2();        //       No         No         Yes
//	runaway_memcmp();           //       No        Yes         Yes
//	load_partial_1();           //      Yes        Yes         3.11
//	load_partial_2();           //      Yes        Yes         Yes
//	write_to_empty_string();    //       No         No          No     ***
//	circumvent_constness();     //       No         No          No     ***
	
	return EXIT_SUCCESS;
}

// =============================================================================
#ifdef BAD_INITIALIZATION_ORDER
/*
 * An instance of BadInitializationOrder uses an element of arr before
 * it is constructed
 */
extern std::string arr[5];

class BadInitializationOrder
{
public:
	BadInitializationOrder() : value(arr[0]) { }
private:
	std::string value;
};

// initialized_before_arr depends on arr and so must be initialized after
BadInitializationOrder initialized_before_arr;

std::string arr[5] = { "Five", "Four", "Three", "Two", "One" };
#endif

